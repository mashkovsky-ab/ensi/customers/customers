CustomerReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: Идентификатор покупателя
      example: 1
    avatar:
      $ref: '../../common_schemas.yaml#/File'
    created_at:
      type: string
      format: date-time
      description: дата создания пользователя
      example: "2021-06-11T11:27:10.000000Z"
    updated_at:
      type: string
      format: date-time
      description: дата обновления пользователя
      example: "2021-06-11T11:27:10.000000Z"
    full_name:
      type: string
      description: "Полное ФИО"
      example: Иванов Иван Иванович
CustomerFillableProperties:
  type: object
  properties:
    user_id:
      type: integer
      description: Идентификатор пользователя
      example: 42
      nullable: true
    status_id:
      type: integer
      description: Идентификатор статуса пользователя
      example: 2
      nullable: true
    manager_id:
      type: integer
      description: Идентификатор менеджера
      example: 4
      nullable: true
    yandex_metric_id:
      type: string
      description: Идентификатор пользователя в YandexMetric
      example: "C001"
      nullable: true
    google_analytics_id:
      type: string
      description: Идентификатор пользователя в GoogleAnalytics
      example: "G001"
      nullable: true
#    attribute_ids:
#      type: array
#      items:
#        type: integer
#      uniqueItems: true
#      description: Массив идентификаторов свойств клиента из Attributes
#      nullable: true
    active:
      type: boolean
      description: Активность пользователя
      example: true
    email:
      type: string
      description: Email
      example: test@mail.com
    phone:
      type: string
      description: Телефон
      example: "+79191000000"
    last_name:
      type: string
      description: Фамилия
      example: Иванов
      nullable: true
    first_name:
      type: string
      description: Имя
      example: Иван
      nullable: true
    middle_name:
      type: string
      description: Отчество
      example: Иванович
      nullable: true
    gender:
      type: integer
      description: Пол из CustomerGenderEnum
      example: 1
      nullable: true
    create_by_admin:
      type: boolean
      description: Пользователь создан администратором
      example: false
    city:
      type: string
      description: Город клиента
      example: "Москва"
      nullable: true
    birthday:
      type: string
      format: date
      description: День рождения
      example: "2020-12-15"
      nullable: true
    last_visit_date:
      type: string
      format: date-time
      description: Дата последней авторизации
      example: "2020-12-15T15:26:12.000000Z"
      nullable: true
    comment_status:
      type: string
      description: Коментарий к статусу клиента
      nullable: true
    timezone:
      type: string
      description: Временная зона
      example: "Europe/Moscow"
CustomerIncludes:
  type: object
  properties:
    addresses:
      type: array
      items:
        $ref: './customer_addresses.yaml#/CustomerAddress'
    status:
      $ref: './customer_statuses.yaml#/CustomerStatuses'
    attributes:
      type: array
      items:
        $ref: './customer_attributes.yaml#/CustomerAttributes'
CustomerRequiredRequestProperties:
  type: object
  required:
    - user_id
    - status_id
    - email
    - phone
    - create_by_admin
    - timezone
CustomerRequiredResponseProperties:
  type: object
  required:
    - user_id
    - status_id
    - manager_id
    - yandex_metric_id
    - google_analytics_id
    - active
    - email
    - phone
    - first_name
    - last_name
    - middle_name
    - gender
    - create_by_admin
    - city
    - birthday
    - last_visit_date
    - comment_status
    - timezone
CustomerForCreate:
  allOf:
    - $ref: '#/CustomerFillableProperties'
    - $ref: '#/CustomerRequiredRequestProperties'
CustomerForReplace:
  allOf:
    - $ref: '#/CustomerFillableProperties'
    - $ref: '#/CustomerRequiredRequestProperties'
CustomerForPatch:
  allOf:
    - $ref: '#/CustomerFillableProperties'
Customer:
  allOf:
    - $ref: '#/CustomerReadonlyProperties'
    - $ref: '#/CustomerFillableProperties'
    - $ref: '#/CustomerIncludes'
    - $ref: '#/CustomerRequiredResponseProperties'

SearchCustomersFilter:
  type: object
  properties:
    id:
      type: integer
      description: Идентификатор покупателя
      example: 1
      nullable: true
    user_id:
      type: integer
      description: Идентификатор пользователя
      example: 42
      nullable: true
    status:
      type: integer
      description: Статус пользователя из Statuses
      example: 1
      nullable: true
    active:
      type: boolean
      description: Активность пользователя
      example: true
      nullable: true
    email:
      type: string
      description: Email
      example: test@mail.com
      nullable: true

SearchCustomersRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    filter:
      $ref: '#/SearchCustomersFilter'
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

CustomerResponse:
  type: object
  properties:
    data:
      $ref: '#/Customer'
    meta:
      type: object
  required:
      - data
SearchCustomersResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Customer'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
      - data
      - meta

VerifyEmailRequest:
  type: object
  properties:
    token:
      type: string
      description: Токен подтерждения почты
      example: 7a8bdac2280617bc66d6d852d5798f4e
      nullable: false
