openapi: 3.0.3
info:
  title: Customers
  contact:
    name: API Support
    url: https://ensi.tech/contacts
    email: mail@greensight.ru
  version: 1.0.0
  description: Управление клиентами
servers:
  - url: /api/v1
tags:
  - name: statuses
    description: Статусы клиентов
  - name: attributes
    description: Характеристики клиентов
  - name: addresses
    description: Адреса клиентов
  - name: customers
    description: Клиенты
paths:
  # Customers
  /customers/customers:search:
    post:
      tags:
        - customers
      operationId: searchCustomers
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@search'
      x-lg-route-name: 'searchCustomers'
      x-lg-skip-request-generation: true
      summary: Поиск объектов типа Customer
      description: Поиск объектов типа Customer
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customers.yaml#/SearchCustomersRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/SearchCustomersResponse'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/customers:search-one:
    post:
      tags:
        - customers
      operationId: searchOneCustomer
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@searchOne'
      x-lg-route-name: 'searchOneCustomers'
      x-lg-skip-request-generation: true
      summary: Поиск объекта типа Customer
      description: Поиск объектов типа Customer
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customers.yaml#/SearchCustomersRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/customers:
    post:
      tags:
        - customers
      operationId: createCustomer
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@create'
      x-lg-route-name: 'createCustomer'
      x-lg-skip-request-generation: true
      summary: Создание объекта типа Customer
      description: Создание объекта типа Customer
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customers.yaml#/CustomerForCreate'
      responses:
        "201":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/customers/{id}:
    get:
      tags:
        - customers
      operationId: getCustomer
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@get'
      x-lg-route-name: 'getCustomer'
      x-lg-skip-request-generation: true
      summary: Получение объекта типа Customer
      description: Получение объекта типа Customer
      parameters:
        - $ref: '#/components/parameters/PathId'
        - $ref: '#/components/parameters/QueryInclude'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    patch:
      tags:
        - customers
      operationId: replaceCustomer
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@patch'
      x-lg-route-name: 'replaceCustomer'
      x-lg-skip-request-generation: true
      summary: Обновление объекта типа Customer
      description: Обновление объекта типа Customer
      parameters:
        - $ref: '#/components/parameters/PathId'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customers.yaml#/CustomerForReplace'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    delete:
      tags:
        - customers
      operationId: deleteCustomer
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@delete'
      x-lg-route-name: 'deleteCustomer'
      x-lg-skip-request-generation: true
      summary: Удаление объекта типа Customer
      description: Удаление объекта типа Customer
      parameters:
        - $ref: '#/components/parameters/PathId'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyDataResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/customers:verify-email:
    post:
      tags:
        - customers
      operationId: verifyEmailCustomer
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\CustomersController@verifyEmail'
      x-lg-route-name: 'verifyEmail'
      x-lg-skip-request-generation: true
      summary: Подтверждение изменения почты
      description: Подтверждение изменения почты
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customers.yaml#/VerifyEmailRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/customers/{id}:upload-avatar:
    post:
      tags:
        - customers
      operationId: uploadCustomerAvatar
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AvatarsController@upload'
      x-lg-route-name: 'uploadCustomerAvatar'
      x-lg-skip-request-generation: true
      summary: Загрузка файла с аватаром покупателя
      description: Загрузка файла с аватаром покупателя
      parameters:
        - $ref: '#/components/parameters/PathId'
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: './common_schemas.yaml#/MultipartFileUploadRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/customers/{id}:delete-avatar:
    post:
      tags:
        - customers
      operationId: deleteCustomerAvatar
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AvatarsController@delete'
      x-lg-route-name: 'deleteCustomerAvatar'
      x-lg-skip-request-generation: true
      summary: Удаление аватара покупателя
      description: Удаление аватара покупателя из базы и файловой системы
      parameters:
        - $ref: '#/components/parameters/PathId'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customers.yaml#/CustomerResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'

  #Addresses
  /customers/addresses:search:
    post:
      tags:
        - addresses
      operationId: searchCustomerAddresses
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AddressesController@search'
      x-lg-route-name: 'searchCustomerAddresses'
      x-lg-skip-request-generation: true
      summary: Поиск объектов типа Customer Address
      description: Поиск объектов типа Customer Address
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_addresses.yaml#/SearchCustomerAddressesRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_addresses.yaml#/SearchCustomerAddressesResponse'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/addresses:
    post:
      tags:
        - addresses
      operationId: createCustomerAddress
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AddressesController@create'
      x-lg-route-name: 'createCustomerAddress'
      x-lg-skip-request-generation: true
      summary: Создание объекта типа CustomerAddress
      description: Создание объекта типа CustomerAddress
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_addresses.yaml#/CustomerAddressForCreate'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_addresses.yaml#/CustomerAddressResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/addresses/{id}:
    get:
      tags:
        - addresses
      operationId: getCustomerAddress
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AddressesController@get'
      x-lg-route-name: 'getCustomerAddress'
      x-lg-skip-request-generation: true
      summary: Получение объекта типа CustomerAddress
      description: Получение объекта типа CustomerAddress
      parameters:
        - $ref: '#/components/parameters/PathId'
        - $ref: '#/components/parameters/QueryInclude'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_addresses.yaml#/CustomerAddressResponse'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    patch:
      tags:
        - addresses
      operationId: replaceCustomerAddress
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AddressesController@patch'
      x-lg-route-name: 'replaceCustomerAddress'
      x-lg-skip-request-generation: true
      summary: Замена объекта типа CustomerAddress
      description: Замена объекта типа CustomerAddress
      parameters:
        - $ref: '#/components/parameters/PathId'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_addresses.yaml#/CustomerAddressForReplace'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_addresses.yaml#/CustomerAddressResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    delete:
      tags:
        - addresses
      operationId: deleteCustomerAddress
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AddressesController@delete'
      x-lg-route-name: 'deleteCustomerAddress'
      x-lg-skip-request-generation: true
      summary: Удаление объекта типа CustomerAddress
      description: Удаление объекта типа CustomerAddress
      parameters:
        - $ref: '#/components/parameters/PathId'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyDataResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/addresses/{id}:set-as-default:
    post:
      tags:
        - addresses
      operationId: setCustomerAddressesAsDefault
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AddressesController@setAsDefault'
      x-lg-route-name: 'setCustomerAddressesAsDefault'
      x-lg-skip-request-generation: true
      summary: Устанавливает Customer Address как адрес по-умолчанию
      description: Устанавливает Customer Address как адрес по-умолчанию
      parameters:
        - $ref: '#/components/parameters/PathId'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyDataResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'

  #Statuses
  /customers/statuses:search:
    post:
      tags:
        - statuses
      operationId: searchCustomerStatuses
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\StatusesController@search'
      x-lg-route-name: 'searchCustomerStatuses'
      x-lg-skip-request-generation: true
      summary: Поиск объектов типа Status
      description: Поиск объектов типа Status
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_statuses.yaml#/SearchCustomerStatusesRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_statuses.yaml#/SearchCustomerStatusesResponse'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/statuses:
    post:
      tags:
        - statuses
      operationId: createCustomerStatus
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\StatusesController@create'
      x-lg-route-name: 'createCustomerStatus'
      x-lg-skip-request-generation: true
      summary: Создание объекта типа Status
      description: Создание объекта типа Status
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_statuses.yaml#/CustomerStatusForCreateOrReplace'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_statuses.yaml#/CustomerStatusResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/statuses/{id}:
    get:
      tags:
        - statuses
      operationId: getCustomerStatus
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\StatusesController@get'
      x-lg-route-name: 'getCustomerStatus'
      x-lg-skip-request-generation: true
      summary: Получение объекта типа Status
      description: Получение объекта типа Status
      parameters:
        - $ref: '#/components/parameters/PathId'
        - $ref: '#/components/parameters/QueryInclude'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_statuses.yaml#/CustomerStatusResponse'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    patch:
      tags:
        - statuses
      operationId: replaceCustomerStatuses
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\StatusesController@patch'
      x-lg-route-name: 'replaceCustomerStatuses'
      x-lg-skip-request-generation: true
      summary: Замена объекта типа Status
      description: Замена объекта типа Status
      parameters:
        - $ref: '#/components/parameters/PathId'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_statuses.yaml#/CustomerStatusForCreateOrReplace'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_statuses.yaml#/CustomerStatusResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    delete:
      tags:
        - statuses
      operationId: deleteCustomerStatus
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\StatusesController@delete'
      x-lg-route-name: 'deleteCustomerStatus'
      x-lg-skip-request-generation: true
      summary: Удаление объекта типа Status
      description: Удаление объекта типа Status
      parameters:
        - $ref: '#/components/parameters/PathId'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyDataResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'

  #Attributes
  /customers/attributes:search:
    post:
      tags:
        - attributes
      operationId: searchCustomerAttributes
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AttributesController@search'
      x-lg-route-name: 'searchCustomerAttributes'
      x-lg-skip-request-generation: true
      summary: Поиск объектов типа Attribute
      description: Поиск объектов типа Attribute
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_attributes.yaml#/SearchCustomerAttributesRequest'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_attributes.yaml#/SearchCustomerAttributesResponse'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/attributes:
    post:
      tags:
        - attributes
      operationId: createCustomerAttributes
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AttributesController@create'
      x-lg-route-name: 'createCustomerAttributes'
      x-lg-skip-request-generation: true
      summary: Создание объекта типа Attribute
      description: Создание объекта типа Attribute
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_attributes.yaml#/CustomerAttributeForCreateOrReplace'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_attributes.yaml#/CustomerAttributeResponse'
        "201":
          description: Элемент создан
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_attributes.yaml#/CustomerAttributeResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "500":
          $ref: '#/components/responses/ServerError'
  /customers/attributes/{id}:
    get:
      tags:
        - attributes
      operationId: getCustomerAttribute
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AttributesController@get'
      x-lg-route-name: 'getCustomerAttribute'
      x-lg-skip-request-generation: true
      summary: Получение объекта типа Attribute
      description: Получение объекта типа Attribute
      parameters:
        - $ref: '#/components/parameters/PathId'
        - $ref: '#/components/parameters/QueryInclude'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_attributes.yaml#/CustomerAttributeResponse'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    patch:
      tags:
        - attributes
      operationId: replaceCustomerAttribute
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AttributesController@patch'
      x-lg-route-name: 'replaceCustomerAttribute'
      x-lg-skip-request-generation: true
      summary: Замена объекта типа Attribute
      description: Замена объекта типа Attribute
      parameters:
        - $ref: '#/components/parameters/PathId'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: './customers/schemas/customer_attributes.yaml#/CustomerAttributeForCreateOrReplace'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: './customers/schemas/customer_attributes.yaml#/CustomerAttributeResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'
    delete:
      tags:
        - attributes
      operationId: deleteCustomerAttribute
      x-lg-handler: 'App\Http\ApiV1\Modules\Customers\Controllers\AttributesController@delete'
      x-lg-route-name: 'deleteCustomerAttribute'
      x-lg-skip-request-generation: true
      summary: Удаление объекта типа Attribute
      description: Удаление объекта типа Attribute
      parameters:
        - $ref: '#/components/parameters/PathId'
      responses:
        "200":
          description: Успешный ответ
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyDataResponse'
        "400":
          $ref: '#/components/responses/BadRequest'
        "404":
          $ref: '#/components/responses/NotFound'
        "500":
          $ref: '#/components/responses/ServerError'


components:
  parameters:
    QueryInclude:
      $ref: './common_parameters.yaml#/QueryInclude'
    PathId:
      $ref: './common_parameters.yaml#/PathId'
  schemas:
    EmptyDataResponse:
      type: object
      properties:
        data:
          type: object
          nullable: true
        meta:
          type: object
      required:
          - data
    PaginationTypeEnum:
      $ref: './common_schemas.yaml#/PaginationTypeEnum'
    CustomerGenderEnum:
      $ref: './customers/enums/customer_gender_enum.yaml'
    MessageForChangeEmail:
      $ref: './kafka_messages.yaml#/MessageForChangeEmail'
  responses:
    BadRequest:
      description: Bad Request
      content:
        application/json:
          schema:
            $ref: './errors.yaml#/ErrorResponse'
    NotFound:
      description: Not Found
      content:
        application/json:
          schema:
            $ref: './errors.yaml#/ErrorResponse'
    ServerError:
      description: Internal Server Error
      content:
        application/json:
          schema:
            $ref: './errors.yaml#/ErrorResponse'
