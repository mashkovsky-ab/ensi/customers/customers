<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CreateStatusAction;
use App\Domain\Customers\Actions\DeleteStatusAction;
use App\Domain\Customers\Actions\ReplaceStatusAction;
use App\Http\ApiV1\Modules\Customers\Queries\StatusQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateOrReplaceStatusRequest;
use App\Http\ApiV1\Modules\Customers\Resources\StatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class StatusesController
{
    public function create(CreateOrReplaceStatusRequest $request, CreateStatusAction $action): StatusesResource
    {
        return new StatusesResource($action->execute($request->validated()));
    }

    public function get(int $statusId,  StatusQuery $query): ?StatusesResource
    {
        return new StatusesResource($query->findOrFail($statusId));
    }

    public function patch(int $statusId, CreateOrReplaceStatusRequest $request, ReplaceStatusAction $action): StatusesResource
    {
        return new StatusesResource($action->execute($statusId, $request->validated()));
    }

    public function delete(int $statusId, DeleteStatusAction $action): EmptyResource
    {
        $action->execute($statusId);

        return new EmptyResource;
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StatusQuery $query): AnonymousResourceCollection
    {
        return StatusesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
