<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CreateAddressAction;
use App\Domain\Customers\Actions\DeleteAddressAction;
use App\Domain\Customers\Actions\ReplaceAddressAction;
use App\Domain\Customers\Actions\SetAddressAsDefaultAction;
use App\Http\ApiV1\Modules\Customers\Queries\AddressesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateOrReplaceAddressRequest;
use App\Http\ApiV1\Modules\Customers\Resources\AddressesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AddressesController
{
    public function create(CreateOrReplaceAddressRequest $request, CreateAddressAction $action): AddressesResource
    {
        return new AddressesResource($action->execute($request->validated()));
    }

    public function get(int $addressId, AddressesQuery $query): AddressesResource
    {
        return new AddressesResource($query->findOrFail($addressId));
    }

    public function patch(int $addressId, CreateOrReplaceAddressRequest $request, ReplaceAddressAction $action): AddressesResource
    {
        return new AddressesResource($action->execute($addressId, $request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, AddressesQuery $query): AnonymousResourceCollection
    {
        return AddressesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function delete(int $addressId, DeleteAddressAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }

    public function setAsDefault(int $addressId, SetAddressAsDefaultAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }
}
