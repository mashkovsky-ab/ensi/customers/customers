<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\DeleteAvatarAction;
use App\Domain\Customers\Actions\SaveAvatarAction;
use App\Http\ApiV1\Modules\Customers\Requests\UploadAvatarRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;

class AvatarsController
{
    public function upload(int $customerId, UploadAvatarRequest $request, SaveAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->file('file')));
    }

    public function delete(int $customerId, DeleteAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId));
    }
}
