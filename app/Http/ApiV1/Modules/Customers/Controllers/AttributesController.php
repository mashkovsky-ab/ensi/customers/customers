<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CreateAttributeAction;
use App\Domain\Customers\Actions\DeleteAttributeAction;
use App\Domain\Customers\Actions\ReplaceAttributeAction;
use App\Http\ApiV1\Modules\Customers\Queries\AttributeQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateOrReplaceAttributeRequest;
use App\Http\ApiV1\Modules\Customers\Resources\AttributesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AttributesController
{
    public function create(CreateOrReplaceAttributeRequest $request, CreateAttributeAction $action): AttributesResource
    {
        return new AttributesResource($action->execute($request->validated()));
    }

    public function get(int $attributeId,  AttributeQuery $query): ?AttributesResource
    {
        return new AttributesResource($query->findOrFail($attributeId));
    }

    public function patch(int $attributeId, CreateOrReplaceAttributeRequest $request, ReplaceAttributeAction $action): AttributesResource
    {
        return new AttributesResource($action->execute($attributeId, $request->validated()));
    }

    public function delete(int $attributeId, DeleteAttributeAction $action): EmptyResource
    {
        $action->execute($attributeId);

        return new EmptyResource;
    }

    public function search(PageBuilderFactory $pageBuilderFactory, AttributeQuery $query): AnonymousResourceCollection
    {
        return AttributesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
