<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\Customer;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class CustomersQuery
 * @package App\Http\ApiV1\Modules\Customers\Queries
 */
class CustomersQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Customer::query();

        parent::__construct($query);

        $this->allowedIncludes(['addresses', 'attributes', 'status']);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('status_id'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('email'),
            AllowedFilter::exact('phone'),
            AllowedFilter::scope('email_like'),
        ]);

        $this->defaultSort('id');
    }
}
