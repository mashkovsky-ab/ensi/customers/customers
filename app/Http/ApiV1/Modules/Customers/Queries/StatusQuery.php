<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\Status;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StatusQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Status::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
        ]);

        $this->defaultSort('id');
    }
}
