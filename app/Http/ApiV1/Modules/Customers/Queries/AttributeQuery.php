<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\Attribute;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class AttributeQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Attribute::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
        ]);

        $this->defaultSort('id');
    }
}
