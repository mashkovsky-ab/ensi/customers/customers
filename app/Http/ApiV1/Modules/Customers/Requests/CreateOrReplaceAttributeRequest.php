<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Domain\Customers\Models\Attribute;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceAttributeRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', Rule::unique(Attribute::class)],
        ];
    }
}
