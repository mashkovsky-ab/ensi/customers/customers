<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Domain\Customers\Models\Attribute;
use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\Status;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class ReplaceCustomerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => ['nullable', 'integer', Rule::unique(Customer::class)->ignore((int)$this->route('id'))],
            'manager_id' => ['nullable', 'integer'],
            'yandex_metric_id' => ['nullable', 'string'],
            'google_analytics_id' => ['nullable', 'string'],
            'email' => ['nullable', 'string', Rule::unique(Customer::class)->ignore((int)$this->route('id'))],
            'phone' => ['nullable', 'regex:/^\+7\d{10}$/', Rule::unique(Customer::class)->ignore((int)$this->route('id'))],
            'first_name' => ['nullable', 'string'],
            'last_name' => ['nullable', 'string'],
            'middle_name' => ['nullable', 'string'],
            'gender' => ['nullable', 'integer'],
            'active' => ['nullable', 'boolean'],
            'create_by_admin' => ['nullable', 'boolean'],
            'avatar' => ['nullable', 'array'],
            'birthday' => ['nullable', 'string'],
            'comment_status' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'last_visit_date' => ['nullable', 'string'],
            'timezone' => ['nullable', 'timezone'],

            'status_id' => ['nullable', 'integer', Rule::exists(Status::class, 'id')],
            'attribute_ids' => ['nullable', 'array'],
            'attribute_ids.*' => ['integer', Rule::exists(Attribute::class, 'id')],
        ];
    }
}
