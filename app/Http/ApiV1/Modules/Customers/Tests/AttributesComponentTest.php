<?php

use App\Domain\Customers\Models\Attribute;
use App\Domain\Customers\Models\Tests\Factories\AttributeFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\post;
use function Pest\Laravel\postJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/customers/attributes/{id}', function () {
    $attribute = AttributeFactory::new()->createOne();

    getJson("/api/v1/customers/attributes/{$attribute->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $attribute->id)
        ->assertJsonPath('data.name', $attribute->name);
});

test('POST /api/v1/customers/attributes/{id}', function () {
    $attribute = AttributeFactory::new()->createOne();

    $request = [
        'name' => $attribute->name,
    ];

    postJson('/api/v1/customers/attributes', $request)
        ->assertStatus(201)
        ->assertJsonPath('data.name', $attribute->name);

    assertDatabaseHas((new Attribute())->getTable(), [
        'name' => $attribute->name,
    ]);
});

test('POST /api/v1/customers/attributes:search', function () {
    $attribute = AttributeFactory::new()->createOne();

    $request = [
        'filter' => [
            'name' => $attribute->name,
        ]
    ];

    postJson('/api/v1/customers/attributes:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $attribute->id)
        ->assertJsonPath('data.0.name', $attribute->name);
});

test('PATCH /api/v1/customers/attributes/{id}', function () {
    $attribute = AttributeFactory::new()->createOne();

    $request = [
        'name' => $attribute->name,
    ];

    patchJson("/api/v1/customers/attributes/{$attribute->id}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $attribute->id)
        ->assertJsonPath('data.name', $attribute->name);

    assertDatabaseHas((new Attribute())->getTable(), [
        'id' => $attribute->id,
        'name' => $attribute->name,
    ]);
});

test('DELETE /api/v1/customers/attributes/{id}', function () {
    $attribute = AttributeFactory::new()->createOne();

    deleteJson("/api/v1/customers/attributes/{$attribute->id}")
        ->assertStatus(200);

    assertDatabaseMissing((new Attribute())->getTable(), [
        'id' => $attribute->id,
        'name' => $attribute->name,
    ]);
});
