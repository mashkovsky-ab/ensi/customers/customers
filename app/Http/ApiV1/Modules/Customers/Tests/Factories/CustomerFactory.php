<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use App\Domain\Customers\Enums\GenderEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\FileFactory;
use Ensi\TestFactories\FactoryMissingValue;

class CustomerFactory extends BaseApiFactory
{
    public ?FileFactory $avatarFactory = null;
    public ?array $addressFactories = null;
    public ?array $statusFactories = null;
    public ?array $attributesFactories = null;

    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'user_id' => $this->faker->optional()->randomNumber(),
            'status_id' => $this->faker->optional()->randomNumber(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'yandex_metric_id' => $this->faker->optional()->randomNumber(),
            'google_analytics_id' => $this->faker->optional()->randomNumber(),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->unique()->phoneNumber(),
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->optional()->text(),
            'gender' => $this->faker->randomElement(GenderEnum::values()),
            'create_by_admin' => $this->faker->boolean(),
            'city' => $this->faker->optional()->text(),
            'birthday' => $this->faker->optional()->date(),
            'comment_status' => $this->faker->optional()->text(),
            'timezone' => $this->faker->timezone(),
            'avatar' => $this->avatarFactory?->make(),
            'new_email' => $this->faker->unique()->email(),
            'email_token' => $this->faker->unique()->text(15),

            'addresses' => $this->executeNested($this->addressFactories, new FactoryMissingValue()),
            'statuses' => $this->executeNested($this->statusFactories, new FactoryMissingValue()),
            'attributes' => $this->executeNested($this->attributesFactories, new FactoryMissingValue()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withAvatar(FileFactory $factory = null): self
    {
        return $this->immutableSet('avatarFactory', $factory ?? FileFactory::new());
    }

    public function includesAddresses(?array $factories = null): self
    {
        return $this->immutableSet('addressFactories', $factories ?? [CustomerAddressFactory::new()]);
    }

    public function includesStatuses(?array $factories = null): self
    {
        return $this->immutableSet('statusFactories', $factories ?? StatusFactory::new());
    }

    public function includesAttributes(?array $factories = null): self
    {
        return $this->immutableSet('attributesFactories', $factories ?? [AttributeFactory::new()]);
    }
}
