<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class CustomerAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'customer_id' => $this->faker->randomNumber(),
            'address_string' => $this->faker->address(),
            'post_index' => $this->faker->optional()->randomNumber(),
            'country_code' => $this->faker->optional()->randomNumber(),
            'region' => $this->faker->optional()->text(2),
            'region_guid' => $this->faker->optional()->text(),
            'area' => $this->faker->optional()->text(),
            'area_guid' => $this->faker->optional()->text(),
            'city' => $this->faker->optional()->text(),
            'city_guid' => $this->faker->optional()->text(),
            'street' => $this->faker->optional()->text(),
            'house' => $this->faker->optional()->numerify('##'),
            'block' => $this->faker->optional()->numerify('##'),
            'porch' => $this->faker->optional()->numerify('##'),
            'intercom' => $this->faker->optional()->numerify('##'),
            'floor' => $this->faker->optional()->numerify('##'),
            'flat' => $this->faker->optional()->numerify('###'),
            'comment' => $this->faker->optional()->text(),
            'geo_lat' => $this->faker->optional()->numerify('##'),
            'geo_lon' => $this->faker->optional()->numerify('##'),
            'default' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
