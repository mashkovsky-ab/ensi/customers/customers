<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class AttributeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'name' => $this->faker->text(),
        ];
    }

    public function make(array $extra = [])
    {
        return $this->makeArray($extra);
    }
}
