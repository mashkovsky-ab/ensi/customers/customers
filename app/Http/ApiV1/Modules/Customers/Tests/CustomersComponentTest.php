<?php

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerUpdatedEventAction;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\CustomerFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\post;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customers:verify-email', function () {
    $this->mock(SendCustomerUpdatedEventAction::class)->shouldReceive('execute');

    $customer = Customer::factory()->createOne();
    $customer->generateEmailToken($customer->new_email);
    $customer->save();
    $request = [
        'token' => $customer->email_token,
    ];

    assertDatabaseHas('customers', [
        'email_token' => $customer->email_token,
    ]);

    postJson('/api/v1/customers/customers:verify-email', $request)
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'user_id', 'active', 'email', 'phone']])
        ->assertJsonPath('data.email', $customer->new_email);
});

/*test('POST /api/v1/customers/customers', function () {
    $customerData = CustomerFactory::new()->make();

    postJson('/api/v1/customers/customers', $customerData)
        ->assertStatus(201)
        ->assertJsonPath('data.user_id', $customerData['user_id']);

    assertDatabaseHas('customers', [
        'user_id' => $customerData['user_id'],
        'status' => $customerData['status'],
    ]);
});*/
