<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\Address;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class AddressesResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 *
 * @mixin Address
 */
class AddressesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge($this->address, [
            'id' => $this->id,
            'default' => $this->default,
            'customer_id' => $this->customer_id,
        ]);
    }
}
