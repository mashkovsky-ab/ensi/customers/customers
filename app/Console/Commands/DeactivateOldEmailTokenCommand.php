<?php

namespace App\Console\Commands;

use App\Domain\Customers\Actions\DeactivateEmailTokenAction;
use Illuminate\Console\Command;

class DeactivateOldEmailTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'email:deactivate-token';

    /**
     * The console command description.
     */
    protected $description = 'Деактивация токенов изменения почты, срок жизни которых истек';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(DeactivateEmailTokenAction $action)
    {
        $action->execute();
    }
}
