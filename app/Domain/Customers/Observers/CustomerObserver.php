<?php

namespace App\Domain\Customers\Observers;

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerUpdatedEventAction;

class CustomerObserver
{
    public function __construct(private SendCustomerUpdatedEventAction $sendCustomerUpdatedEventAction)
    {
        //
    }

    public function updated(Customer $customer)
    {
        $this->sendCustomerUpdatedEventAction->execute($customer);
    }
}
