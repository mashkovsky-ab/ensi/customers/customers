<?php

namespace App\Domain\Customers\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self FEMALE()
 * @method static self MALE()
 */
class GenderEnum extends Enum
{
    public static function values(): array
    {
        return [
            'FEMALE' => 1,
            'MALE' => 2,
        ];
    }
}
