<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\AddressFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Адрес покупателя"
 *
 * Class Address
 * @package App\Domain\Customers\Models
 *
 * @property int $id
 * @property int $customer_id - ид покупателя
 * @property array $address - информация об адресе
 * @property bool $default - адрес по умолчанию
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Address extends Model
{
    /**
     * @var string
     */
    protected $table = 'customer_addresses';

    protected $casts = [
        'address' => 'array',
        'default' => 'boolean',
    ];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public static function factory()
    {
        return AddressFactory::new();
    }
}
