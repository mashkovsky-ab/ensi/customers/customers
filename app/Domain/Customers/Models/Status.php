<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\StatusFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Статус покупателя"
 *
 * Class Status
 * @package App\Domain\Customers\Models
 *
 * @property int $id
 * @property string $name
 */
class Status extends Model
{
    /**
     * @var string
     */
    protected $table = 'statuses';

    public static function factory(): StatusFactory
    {
        return StatusFactory::new();
    }
}
