<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /** @inheritdoc  */
    protected $model = Address::class;

    /** @inheritdoc  */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->randomNumber(),
            'address' => [
                'address_string' => $this->faker->optional()->text(15),
                'post_index' => $this->faker->optional()->randomNumber(),
                'country_code' => $this->faker->optional()->randomNumber(),
                'region' => $this->faker->optional()->text(5),
                'region_guid' => $this->faker->optional()->text(),
                'area' => $this->faker->optional()->text(),
                'area_guid' => $this->faker->optional()->text(),
                'city' => $this->faker->optional()->text(),
                'city_guid' => $this->faker->optional()->text(),
                'street' => $this->faker->optional()->text(),
                'house' => $this->faker->optional()->numerify('##'),
                'block' => $this->faker->optional()->numerify('##'),
                'porch' => $this->faker->optional()->numerify('##'),
                'intercom' => $this->faker->optional()->numerify('##'),
                'floor' => $this->faker->optional()->numerify('##'),
                'flat' => $this->faker->optional()->numerify('###'),
                'comment' => $this->faker->optional()->text(),
                'geo_lat' => $this->faker->optional()->numerify('##'),
                'geo_lon' => $this->faker->optional()->numerify('##'),
            ],
            'default' => $this->faker->boolean(),
        ];
    }
}
