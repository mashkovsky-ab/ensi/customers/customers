<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Attribute;
use Illuminate\Database\Eloquent\Factories\Factory;

class AttributeFactory extends Factory
{
    /** @inheritdoc  */
    protected $model = Attribute::class;

    /** @inheritdoc  */
    public function definition()
    {
        return [
            'name' => $this->faker->text(20),
        ];
    }
}
