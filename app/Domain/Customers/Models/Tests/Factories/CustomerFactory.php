<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Enums\GenderEnum;
use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method Customer createOne(array $fields = [])
 */
class CustomerFactory extends Factory
{
    /** @inheritdoc */
    protected $model = Customer::class;

    /** @inheritdoc  */
    public function definition()
    {
        return [
            'user_id' => 1,
            'status_id' => Status::factory(),
            'manager_id' => 1,
            'yandex_metric_id' => $this->faker->randomNumber(),
            'google_analytics_id' => $this->faker->randomNumber(),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->unique()->phoneNumber(),
            'first_name' => $this->faker->text(10),
            'last_name' => $this->faker->text(10),
            'middle_name' => $this->faker->text(10),
            'gender' => $this->faker->randomElement(GenderEnum::values()),
            'create_by_admin' => $this->faker->boolean(),
            'city' => $this->faker->text(),
            'birthday' => $this->faker->date(),
            'comment_status' => $this->faker->text(),
            'timezone' => $this->faker->timezone(),
            'avatar' => null,
            'new_email' => $this->faker->unique()->email(),
        ];
    }

    /**
     * @param string|null $path
     * @return $this
     */
    public function withAvatar(?string $path = null): self
    {
        return $this->state(fn (array $attributes) => [
            'avatar' => $path ?: $this->faker->text(),
        ]);
    }
}
