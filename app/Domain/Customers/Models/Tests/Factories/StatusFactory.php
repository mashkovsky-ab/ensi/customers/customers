<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusFactory extends Factory
{
    /** @inheritdoc  */
    protected $model = Status::class;

    /** @inheritdoc  */
    public function definition()
    {
        return [
            'name' => $this->faker->text(15),
        ];
    }
}
