<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\CustomerFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Класс-модель для сущности "Покупатель"
 *
 * Class Customer
 * @package App\Domain\Customers\Models
 *
 * @property int $id
 * @property int $user_id - id покупателя в сервисе авторизации
 * @property int $status_id - id статуса пользователя
 * @property int $manager_id - id персонального менеджера
 * @property string $yandex_metric_id - Яндекс.Метрика (UserID)
 * @property string $google_analytics_id - Google Analytics (User ID)
 *
 * @property bool $active - Активность пользователя
 * @property string $email - Почта пользователя
 * @property string $phone - Номер телефона пользователя
 * @property string $first_name - Имя
 * @property string $last_name - Фамилия
 * @property string $middle_name - Отчество
 * @property int $gender - Пол
 * @property bool $create_by_admin - Пользователь создан администратором
 * @property string $avatar - Объект с данными об аватаре
 * @property string $city - Город
 * @property Carbon $birthday - День рождения
 * @property Carbon $last_visit_date - Дата последнего визита
 * @property string $comment_status - Комментарий к статусу
 * @property string $timezone
 * @property string $new_email
 * @property string $email_token
 * @property Carbon $email_token_created_at
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read string full_name
 *
 * @property Collection|address[] $addresses
 * @property Status|null $status
 * @property Attribute[]|null $attributes
 */
class Customer extends Model
{
    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'user_id', 'status_id', 'manager_id', 'yandex_metric_id', 'google_analytics_id', 'active', 'email', 'phone',
        'first_name', 'last_name', 'middle_name', 'gender', 'create_by_admin', 'city', 'birthday',
        'last_visit_date', 'comment_status', 'timezone',
    ];

    // Время жизни токена изменения почты в днях
    const LIFE_TIME_EMAIL_TOKEN = 1;

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var string
     */
    protected $table = 'customers';

    /**
     * @var string[]
     */
    protected $dates = ['birthday', 'last_visit_date'];

    /**
     * @var string[]
     */
    protected $casts = [
        'active' => 'boolean',
        'create_by_admin' => 'boolean',
    ];

    /**
     * @return HasMany
     */
    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class);
    }

    /**
     * @return HasOne
     */
    public function status(): HasOne
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    /**
     * @return BelongsToMany
     */
    public function attributes(): BelongsToMany
    {
        return $this->belongsToMany(Attribute::class, 'customer_attributes');
    }

    public function generateEmailToken($new_email): void
    {
        $this->new_email = $new_email;
        $this->email_token = md5($this->email . time());
        $this->email_token_created_at = now();
    }

    public function destroyEmailToken(): void
    {
        $this->new_email = null;
        $this->email_token = null;
        $this->email_token_created_at = null;
    }

    public function getFullNameAttribute(): string
    {
        $pieces = [];
        if ($this->last_name) {
            $pieces[] = $this->last_name;
        }
        if ($this->first_name) {
            $pieces[] = $this->first_name;
        }
        if ($this->middle_name) {
            $pieces[] = $this->middle_name;
        }

        return implode(' ', $pieces);
    }

    /**
     * @param Builder $query
     * @param string $email
     * @return Builder
     */
    public function scopeEmailLike(Builder $query, string $email): Builder
    {
        return $query->where('email', 'ilike', "%{$email}%");
    }

    /**
     * @return CustomerFactory
     */
    public static function factory(): CustomerFactory
    {
        return CustomerFactory::new();
    }

    /**
     * Find user by email
     *
     * @param string $email
     * @return Customer
     */
    public static function findByEmail(string $email): Customer
    {
        return Customer::where('email', $email)->first();
    }

    /**
     * Find user by phone
     *
     * @param string $phone
     * @return Customer
     */
    public static function findByPhone(string $phone): Customer
    {
        return Customer::where('phone', $phone)->first();
    }
}
