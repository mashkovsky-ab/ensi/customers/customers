<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\AttributeFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Класс-модель для сущности "Признак покупателя"
 *
 * Class Attribute
 * @package App\Domain\Customers\Models
 *
 * @property $id
 * @property $name
 */
class Attribute extends Model
{
    /**
     * @var string
     */
    protected $table = 'attributes';

    /**
     * @return BelongsToMany
     */
    public function customers(): BelongsToMany
    {
        return $this->belongsToMany(Customer::class, 'customer_attributes');
    }

    public static function factory()
    {
        return AttributeFactory::new();
    }
}
