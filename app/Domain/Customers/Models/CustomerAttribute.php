<?php

namespace App\Domain\Customers\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerAttribute
 * @package App\Domain\Customers\Models
 */
class CustomerAttribute extends Model
{
    /**
     * @var string
     */
    protected $table = 'customer_attributes';
}
