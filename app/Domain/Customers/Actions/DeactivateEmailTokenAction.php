<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Customer;

class DeactivateEmailTokenAction
{
    public function execute(): void
    {
        $customersWithOldEmailToken =  Customer::query()
            ->where('email_token_created_at', '<', now()->subDays(Customer::LIFE_TIME_EMAIL_TOKEN))
            ->get();
        if (!$customersWithOldEmailToken->isEmpty()) {
            $customersWithOldEmailToken->each(function (Customer $customer) {
                $customer->destroyEmailToken();
                $customer->save();
            });
        }
    }
}
