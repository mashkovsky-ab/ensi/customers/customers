<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Status;

class ReplaceStatusAction
{
    public function execute(int $statusId, array $fields): Status
    {
        $status = Status::findOrFail($statusId);
        $status->name = $fields['name'];

        $status->save();

        return $status;
    }
}
