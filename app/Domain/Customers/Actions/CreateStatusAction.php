<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Status;

class CreateStatusAction
{
    public function execute(array $fields): Status
    {
        $status = new Status();
        $status->name = $fields['name'];

        $status->save();

        return $status;
    }
}
