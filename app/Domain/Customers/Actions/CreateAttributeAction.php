<?php


namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Attribute;

class CreateAttributeAction
{
    public function execute(array $fields): Attribute
    {
        $attribute = new Attribute();
        $attribute->name = $fields['name'];

        $attribute->save();

        return $attribute;
    }
}
