<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Attribute;

class DeleteAttributeAction
{
    public function execute(int $attributeId): void
    {
        Attribute::destroy($attributeId);
    }
}
