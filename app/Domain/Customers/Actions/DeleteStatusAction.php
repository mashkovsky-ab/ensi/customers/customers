<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Status;

class DeleteStatusAction
{
    public function execute(int $statusId): void
    {
        Status::destroy($statusId);
    }
}
