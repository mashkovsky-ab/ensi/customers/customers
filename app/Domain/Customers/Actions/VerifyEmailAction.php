<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Customer;

class VerifyEmailAction
{
    public function execute($fields): Customer
    {
        $token = $fields['token'];
        /** @var Customer $customer */
        $customer = Customer::query()->where('email_token', $token)->firstOrFail();

        $customer->email = $customer->new_email;
        $customer->destroyEmailToken();
        $customer->save();

        return $customer;
    }
}
