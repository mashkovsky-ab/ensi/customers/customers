<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Customer;
use Illuminate\Support\Arr;

class CreateCustomerAction
{
    public function execute(array $fields): Customer
    {
        /** @var Customer $customer */
        $customer = Customer::create(Arr::only($fields, Customer::FILLABLE));
        if (!empty($fields['attribute_ids'])) {
            $customer->attributes()->attach($fields['attribute_ids']);
        }

        return $customer;
    }
}
