<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Customer;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;

class ReplaceCustomerAction
{
    public function execute(int $customerId, array $fields): Customer
    {
        /** @var Customer $customer */
        $customer = Customer::findOrFail($customerId);

        // Если пришел новый email, то отправляем письмо на подтверждение
        if (array_key_exists('email', $fields)) {
            $newEmail = $fields['email'];
            unset($fields['email']);
            $this->refreshToken($customer, $newEmail);
        }

        $customer->update($fields);

        if (array_key_exists('attribute_ids', $fields)) {
            $customer->attributes()->sync($fields['attribute_ids']);
        }

        return $customer;
    }

    public function refreshToken(Customer $customer, string $newEmail)
    {
        $customer->generateEmailToken($newEmail);
        $customer->save();

        $message = json_encode([
            'customer_id' => $customer->id,
            'token' => $customer->email_token,
            'new_email' => $customer->new_email,
        ]);
        (new HighLevelProducer(config('kafka.topics.changes-email')))
            ->sendOne($message);
    }
}
