<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Attribute;

class ReplaceAttributeAction
{
    public function execute(int $attributeId, array $fields): Attribute
    {
        $attribute = Attribute::findOrFail($attributeId);
        $attribute->name = $fields['name'];

        $attribute->save();

        return $attribute;
    }
}
