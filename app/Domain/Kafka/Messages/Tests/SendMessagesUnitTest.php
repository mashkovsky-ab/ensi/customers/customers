<?php

use App\Domain\Kafka\Messages\Send\CustomerUpdatedEventMessage;
use App\Domain\Customers\Models\Customer;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region CustomerUpdatedEventMessage
//test("generate CustomerUpdatedEventMessage success", function () {
//    /** @var Customer $customer */
//    $customer = Customer::factory()->makeOne();
//    $customer->save();
//
//    $oldActive = $customer->first_name;
//    $customer->first_name = 'test';
//
//    $message = new CustomerUpdatedEventMessage($customer);
//    $messageArray = $message->toArray();
//    assertIsArray($messageArray);
//    assertEquals($messageArray['old_data']['first_name'], $oldActive);
//});
// endregion
