<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\CustomerUpdatedEventMessage;
use App\Domain\Customers\Models\Customer;

class SendCustomerUpdatedEventAction extends SendMessageAction
{
    public function execute(Customer $customer)
    {
        $event = new CustomerUpdatedEventMessage($customer);
        $this->send($event);
    }
}
