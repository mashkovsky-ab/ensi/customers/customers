<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\KafkaMessage;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;

abstract class SendMessageAction
{
    protected function send(KafkaMessage $message)
    {
        (new HighLevelProducer($message->topicName()))->sendOne(json_encode($message->toArray()));
    }
}
